/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controlador;

import Model.Calculainadooor;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Vermi
 */
public class DatosForm extends HttpServlet {


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)        
        throws ServletException, IOException {
            response.setContentType("text/html;charset=UTF-8");
           
            /*int Capital1=Integer.parseInt(request.getParameter("Capital"));
            int Interes1=Integer.parseInt(request.getParameter("Interes"));
            int Años1=Integer.parseInt(request.getParameter("Años"));
        
            Calculainadooor calculo = new Calculainadooor();
            request.setAttribute("Nombre",request.getParameter("Nombre") );
            request.setAttribute("total", calculo.Calculainadooor(Capital1, Interes1, Años1));
            
            RequestDispatcher rd = request.getRequestDispatcher("/final.jsp");
            rd.forward(request, response);
            response.sendRedirect("/final.jsp");
            request.getRequestDispatcher("/final.jsp").forward(request, response);*/
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            Calculainadooor calculo = new Calculainadooor();
            int capital = Integer.parseInt(request.getParameter("Capital"));
            int interes = Integer.parseInt(request.getParameter("Interes"));
            int edad= Integer.parseInt(request.getParameter("Edad"));
            
            request.setAttribute("total", calculo.Calculainador(capital, interes, edad));
            request.setAttribute("Nombre1", request.getParameter("Nombre"));
            request.getRequestDispatcher("/final.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
